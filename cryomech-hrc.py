import os
import sys
import time
from datetime import datetime, timezone, timedelta

import serial

import argparse

parser = argparse.ArgumentParser(description="connects to cryomech helium recovery controller to monitor pressure and heating")
parser.add_argument("--port", "-p",  type=str, help="com port to connect to")
parser.add_argument("--unit", "-u",  type=str, help="which cryomech unit we are connecting to (1, 2, B)")
parser.add_argument("--config", type=str, default="onnes", help="influx .ini configuration file to load")

args = parser.parse_args()

# get current folder to get abs. path to config file
import pathlib
cpath = pathlib.Path(__file__).parent.absolute() / f"{args.config}.ini"


#
# InfluxDB configuration


from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "data"
client = InfluxDBClient.from_config_file(cpath)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()


# use serial communication to talk to mercury iTC
import serial
ser = serial.Serial(args.port, timeout=0.1)


def readlines():
    """
    read all lines available until timeout occurs
    """
    lines = []
    line = ser.readline()
    while len(line) > 0:
        lines.append(line.decode())
        line = ser.readline()
    
    return lines


# the HRC uses a weird interactive interface.  Start by sending line end and clearing input
ser.write(b'\n')
time.sleep(0.1)
lines = readlines()
if len(lines) < 5:
    print("Expected longer response from HRC:")
    for l in lines:
        print(l)
    sys.exit(1)


# Turn on monitor mode with 'T'.  But check response in case we turned it off.
ser.write(b'T')
time.sleep(0.5)
lines = readlines()
if lines[0].find('Pressure') < 0:
    # We turned off monitoring.  Turn it on again
    print("Monitor was already active.  Reactivating")
    ser.write(b'T')
    time.sleep(0.5)
    lines = readlines()

# There should be a pressure reading in lines
if lines[0].find('Pressure') < 0:
    print("Expected a pressure measurement:")
    for l in lines:
        print(l)
    sys.exit(1)
else:
    print("HRC connected OK")

# Now the HRC should be sending status updates over the serial port
# every ~second.  Monitor and report to database

try:
    while True:
        lines = readlines()
        
        if len(lines) > 0:
            splits = lines[-1].strip().split()
            pressure = float(splits[1])
            power = float(splits[3])

            print(f"Pressure: {pressure}, Power: {power}")
            sys.stdout.flush()

            p = Point('cryomech-hrc') \
                .tag('unit', args.unit) \
                .field('pressure (psi)', pressure) \
                .field('power (W)', power)

            write_api.write(bucket=bucket, record=[p])

        time.sleep(10)
except KeyboardInterrupt:
    # use try-clause to catch exit signal and turn off monitoring from HRC
    print("Exiting...")
    ser.write(b'T')
    ser.close()
