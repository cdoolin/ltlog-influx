import os
import sys
import time
from datetime import datetime, timezone, timedelta

import serial

import argparse

parser = argparse.ArgumentParser(description="connects to cryomech to monitor He level")
parser.add_argument("--port", "-p",  type=str, help="com port to connect to")
parser.add_argument("--config", type=str, default="onnes", help="influx .ini configuration file to load")

args = parser.parse_args()

# get current folder to get abs. path to config file
import pathlib
cpath = pathlib.Path(__file__).parent.absolute() / f"{args.config}.ini"


#
# InfluxDB configuration


from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "data"
client = InfluxDBClient.from_config_file(cpath)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()


# use serial communication to talk to mercury iTC
import serial
ser = serial.Serial(args.port, baudrate=9600, timeout=1)

def ask(q):
    ser.write(q)
    # echoes command, so read it first
    ser.readline()
    return ser.readline().decode().strip()

def write(q):
    ser.write(q)
    ser.readline()

# Get serial number to find out which unit is connected
cryomechs = {
    '5389': '2',
    '5802': '1',
    '4323': 'B',
}

cals = {
    '1': [7.33, -10.9],
    '2': [7.33, -10.9],
    'B': [7.33, -10.9],
}

info = ask(b"*IDN?\n").split(',')
if info[1] != 'LM-500':
    print("Wrong device connected:")
    print(ask)
    sys.exit(1)

unit = cryomechs[info[2]]
print(f"Connected to cryomech '{unit}'")

# make sure we are in inches
ser.write(b"UNITS IN\n")

last_level = None

while True:
    meas = ask(b'MEAS?\n')
    meas_split = meas.split()

    if meas_split[1] != 'in':
        print(f"invalid units in measurement: {meas}")
        level = last_level
    else:
        level = float(meas_split[0])
        liters = level * cals[unit][0] + cals[unit][1]

    print(f"{unit}: {level}")

    if level != last_level:
        p = Point("cryomech")\
            .tag("unit", unit) \
            .field("level", level) \
            .field("liters", liters)

        write_api.write(bucket=bucket, record=p)

        last_level = level

    time.sleep(600)


    


