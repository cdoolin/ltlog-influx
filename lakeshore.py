
#%%

import os
import sys
import time
from datetime import datetime, timezone, timedelta


import argparse

parser = argparse.ArgumentParser(description="reads lakeshore measurements and sends to influxdb")
parser.add_argument("--every", "-e", type=float, default=1, help="delay in seconds between checking the file (default 1)")
parser.add_argument("--config", type=str, default="onnes", help="influx .ini configuration file to load")

args = parser.parse_args()


#%%
import pathlib
cpath = pathlib.Path(__file__).parent.absolute() / f"{args.config}.ini"
cpath
# %%
# InfluxDB configuration
#

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "data"
client = InfluxDBClient.from_config_file(cpath)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()

#
# Setup lakeshore
#
import pyvisa
rm = pyvisa.ResourceManager()
LakeShore = rm.open_resource('GPIB::12::INSTR')


channels = ["Still", "Coldplate", "Demag mag.", "Sample mag.", "Mixing chamber"]


while True:
    channel, autoscan = LakeShore.query("scan?").strip().split(',')
    channel = int(channel)
    
    if autoscan == '0':
        print("warning, autoscan is off")
        sys.stdout.flush()
        
    if channel in [1, 2, 3, 4, 5]:
        data = LakeShore.query("RDGR? %d" % channel).strip()
        dataT = LakeShore.query("RDGK? %d" % channel).strip()
        
        point = Point("lakeshore") \
            .tag("fridge", "oxford") \
            .tag("channel", channels[channel - 1]) \
            .field("resistance", float(data)) \
            .field("temperature", float(dataT))
        write_api.write(bucket=bucket, record=[point])

        print("channel %d: %f Ohm  %f K" % (channel, float(data), float(dataT)))
        sys.stdout.flush()
    else:
        print("wasn't expecting data from channel %d" % channel)
        sys.stdout.flush()

    time.sleep(args.every)