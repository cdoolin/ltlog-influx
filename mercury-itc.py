
#%%

import os
import sys
import time
from datetime import datetime, timezone, timedelta


import argparse

parser = argparse.ArgumentParser(description="watches oxford fridge log files and reports changes to influxdb")
parser.add_argument("--port", "-p",  type=str, default="COM4")
parser.add_argument("--every", "-e", type=float, default=1, help="delay in seconds between checking the file (default 1)")
parser.add_argument("--config", type=str, default="onnes", help="influx .ini configuration file to load")

args = parser.parse_args()


#%%
import pathlib
cpath = pathlib.Path(__file__).parent.absolute() / f"{args.config}.ini"
cpath
# %%
# InfluxDB configuration
#

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "data"
client = InfluxDBClient.from_config_file(cpath)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()




# use serial communication to talk to mercury iTC
import serial
s = serial.Serial(args.port, timeout=1)

value0 = None
while True:
    
    s.write(b"READ:DEV:DB5.L1:LVL:SIG:HEL:LEV\n")
    m = s.readline().decode()

    s.write(b"READ:DEV:DB5.L1:LVL:SIG:HEL:RES\n")
    mR = s.readline().decode()

    try:
        val = m[m.rindex(":") + 1:m.rindex("%")]
        val = float(val)

        valR = mR[mR.rindex(":") + 1:mR.rindex("O")]
        valR = float(valR)
    except ValueError:
        print("bad response: %s" % m)
        sys.stdout.flush()
        val = value0
        
    if val != value0:
        print(f"{val} %, {valR} Ohm")
        sys.stdout.flush()
        point = Point("mercury-itc") \
            .field("he_level (%)", float(val)) \
            .field("he_level (Ω)", float(valR))
        write_api.write(bucket=bucket, record=[point])

        value0 = val
        
    time.sleep(args.every)



