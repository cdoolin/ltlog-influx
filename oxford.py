
#%%

import os
import sys
import time
from datetime import datetime, timezone, timedelta




try:
    get_ipython()
    in_notebook = True
except NameError:
    in_notebook = False

if not in_notebook:
    import argparse

    parser = argparse.ArgumentParser(description="watches oxford fridge log files and reports changes to influxdb")
    parser.add_argument("path", type=str, help="path to file to watch")
    parser.add_argument("--every", "-e", type=float, default=1, help="delay in seconds between checking the file (default 1)")
    parser.add_argument("--config", type=str, default="onnes", help="influx .ini configuration file to load")

    args = parser.parse_args()
else:
    class dotdict(dict):
        __getattr__ = dict.get
        __setattr__ = dict.__setitem__
        __delattr__ = dict.__delitem__
    # for running interactivly
    args = dotdict(
        path="c:/Oxford Fridge Log/callumrun_december_2020.txt",
        config="onnes",
        every=1,
    )



if not os.path.exists(args.path):
    print("%s doesn't exit" % args.path)
    sys.exit(1)


#%%
import pathlib
cpath = pathlib.Path(__file__).parent.absolute() / f"{args.config}.ini"
cpath

# %%
# InfluxDB configuration

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "data"
client = InfluxDBClient.from_config_file(cpath)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()

# %%
# Find most recent timestamp, and upload everything else
q = f"""
from(bucket: "{bucket}")
  |> range(start:-20d)
  |> filter(fn: (r) => r["_measurement"] == "oxfordlog" and r["_field"] == "Htr. M/C (uW)")
  |> top(n:1, columns: ["_time"])
"""
tables = query_api.query(q)

if len(tables) == 0:
    lasttime = datetime.fromtimestamp(0, tz=timezone.utc)
else:
    lasttime = tables[-1].records[0].get_time()
print(f"last update of database: {lasttime}")
sys.stdout.flush()

# %%
# Load oxford configuration file

logfile = open(args.path, 'r')

# read timestamp at beginning of file
# times in log file are seconds after the file was created
# creation labview timestamp in first line of logfile
timestr = logfile.readline()
labview_ts = int(timestr[timestr.find("-") + 1:])
# Use labview epoch to parse timestamp
labview_epoch = datetime(1904, 1, 1, 0, 0, 0, tzinfo=timezone.utc)
logstart = labview_epoch + timedelta(seconds=labview_ts)
print(f"oxford logfile started {logstart} UTC")
sys.stdout.flush()

# 5 extra lines in header.
for i in range(5):
    logfile.readline()

fields = [
    "Press. G1 (mBar)",
    "Press. G2 (mBar)",
    "Press. G3 (mBar)",
    "Press. P1 (mBar)",
    "Press. P2 (mBar)",
    "Htr. M/C (uW)",
    "Htr. Still (mW)",
    "Htr. Sorb (mW)",
    "Valve V12A (%)",
    "Valve V6 (%)",
    "Valve V1K (%)",
    "Temp. M/C (K)",
    "Temp. 1K pot (K)",
    "Temp. Sorb (K)",
]

# %%

def read_point():
    time = lasttime
    while time <= lasttime:
        line = logfile.readline().strip().split('\t')
        if len(line) < len(fields) + 1:
            return None
        time = logstart + timedelta(seconds=int(line[0]))

    vals = [float(v) for v in line[1:]]

    if len(vals) != len(fields):
        raise RuntimeError("Number of values not equal to expected number of fields")
    
    point = Point("oxfordlog") \
        .time(time, WritePrecision.S)

    for f, v in zip(fields, vals):
        point.field(f, v)

    return point

def load_points(maxpoints=5000):
    """
    load up to maxpoints from log file.  Used since submitting many points individually can be slow.
    """
    record = []
    point = read_point()

    while point is not None and len(record) < maxpoints:
        record.append(point)
        point = read_point()

    return record
# %%




# %%


# record = load_points()
# while len(record) > 0:

#     try:   
#         write_api.write(bucket=bucket, record=record)
#     except:
#         print("unable to connect to server... trying again in 5 s")
#         time.sleep(5)
#         continue
#     print(record[-1]._time)
#     record = load_points()

#%%

record = load_points()
while True:
    try:   
        write_api.write(bucket=bucket, record=record)
    except:
        print("unable to connect to server...")
        time.sleep(args.every)
        continue
    print(record[-1]._time)
    sys.stdout.flush()

    record = []
    while len(record) < 1:
        time.sleep(args.every)
        record = load_points()



# 
# %%
